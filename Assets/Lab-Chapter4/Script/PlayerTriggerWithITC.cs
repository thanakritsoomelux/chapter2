using System.Collections;
using System.Collections.Generic;
using Thanakrit.GameDev3;
using UnityEngine;


public class PlayerTriggerWithITC : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        ItemTypeComponent itc = other.GetComponent <ItemTypeComponent >();
        var inventory = GetComponent <Inventory >();
        var simpleHp = GetComponent <SimpleHealthPointComponent >();

        if (itc != null)
        {
            switch (itc.Type)
            {
                case ItemType.COIN:
                    inventory.AddItem("COIN",1);
                    break;
                case ItemType.BIGCOIN:
                    inventory.AddItem("BIGCOIN",1);
                    break;
                case ItemType.HEALING:
                    if(simpleHp != null)
                        simpleHp.HealthPoint = simpleHp.HealthPoint + 100;
                    break;
                case ItemType.TOXIC:
                    if(simpleHp != null)
                        simpleHp.HealthPoint = simpleHp.HealthPoint - 100;
                    break;
                case ItemType.POWERUP:
                    if(simpleHp != null)
                        simpleHp.HealthPoint = simpleHp.HealthPoint + 10;
                    break;
                case ItemType.POWERDOWN:
                    if(simpleHp != null)
                        simpleHp.HealthPoint = simpleHp.HealthPoint - 10;
                    break;
            }
        }
        Destroy(other.gameObject ,0);
    }
}
