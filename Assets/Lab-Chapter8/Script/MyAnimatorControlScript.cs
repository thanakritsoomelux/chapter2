using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAnimatorControlScript : MonoBehaviour
{
    protected Animator m_Animator;

    private static readonly int Punch = Animator.StringToHash("Punch");
    private static readonly int Dancing = Animator.StringToHash("Dancing");
    private static readonly int Crouch = Animator.StringToHash("Crouch");
    private static readonly int Turn = Animator.StringToHash("Turn");
    private static readonly int Forward = Animator.StringToHash("Forward");
    private static readonly int DrunkMode = Animator.StringToHash("DrunkMode");



    private void Start()
    {
        m_Animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_Animator.SetTrigger("Jump");
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            m_Animator.SetTrigger(Punch);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetTrigger(DrunkMode);
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            m_Animator.SetTrigger(Dancing);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            m_Animator.SetBool(Crouch, true);
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            m_Animator.SetBool(Crouch, false);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            m_Animator.SetFloat(Forward, 0.5f);
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            m_Animator.SetFloat(Forward, 0.0f);
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            m_Animator.SetFloat(Forward, 1f);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            m_Animator.SetFloat(Forward, 0f);
        }
        
        if (Input.GetKeyDown(KeyCode.A))
        {
            m_Animator.SetFloat(Turn, -0.5f);
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            m_Animator.SetFloat(Turn, 0.0f);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            m_Animator.SetFloat(Turn, 0.5f);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            m_Animator.SetFloat(Turn, 0.0f);
        }
    }
}
