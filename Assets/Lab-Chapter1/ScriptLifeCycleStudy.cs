using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Thanakrit.GameDev3
{
    public class ScriptLifeCycleStudy : MonoBehaviour
    {
        private int m_CallingSequence = 1;

        private void Awake()
        {
            Debug.LogFormat("{0} {1} has been called.",m_CallingSequence++,MethodBase.GetCurrentMethod().Name);
        }

        private void Start()
        {
            Debug.LogFormat("{0} {1} has been called.",m_CallingSequence++,MethodBase.GetCurrentMethod().Name);
        }

        void Update()
        {
        
        }

        private void FixedUpdate()
        {
        
        }

        private void OnEnable()
        {
            Debug.LogFormat("{0} {1} has been called.",m_CallingSequence++,MethodBase.GetCurrentMethod().Name);
        }

        private void OnDisable()
        {
            Debug.LogFormat("{0} {1} has been called.",m_CallingSequence++,MethodBase.GetCurrentMethod().Name);
        }

        private void OnDestroy()
        {
            Debug.LogFormat("{0} {1} has been called.",m_CallingSequence++,MethodBase.GetCurrentMethod().Name);
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            Debug.LogFormat("{0} {1} has been called.",m_CallingSequence++,MethodBase.GetCurrentMethod().Name);
        }

        private void OnApplicationQuit()
        {
            Debug.LogFormat("{0} {1} has been called.",m_CallingSequence++,MethodBase.GetCurrentMethod().Name);
        }
    }
}

