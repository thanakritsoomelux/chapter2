using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyHistory : MonoBehaviour
{
    private List<string> keyboardInputs;
    private float elapsedTime;
    
    private float Duration = 10f;
    private float keyCheck = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        keyboardInputs = new List<string>();
        StartCoroutine(CheckKeyboardInputs());
    }

    private IEnumerator CheckKeyboardInputs()
    {
        while (true)
        {
            yield return new WaitForSeconds(keyCheck);
            elapsedTime += keyCheck;



            if (elapsedTime > Duration)
                RemoveOldKeyboardInputs();



            foreach (KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKey(keyCode))
                {
                    string keyboardInput = keyCode.ToString();
                    AddKeyboardInputToHistory(keyboardInput);
                }
            }
        }
    }


    private void AddKeyboardInputToHistory(string keyboardInput)
    {
        if (keyboardInputs.Contains(keyboardInput))
            return;

 

        if (keyboardInputs.Count >= 5)
            keyboardInputs.RemoveAt(0);

 

        keyboardInputs.Add(keyboardInput);
    }

 

    private void RemoveOldKeyboardInputs()
    {
        keyboardInputs.Clear();
        elapsedTime = 0f;
    }

 

    // ReSharper disable Unity.PerformanceAnalysis
    public void PrintKeyboardInputHistory()
    {
        Debug.Log("Keyboard Input History:");
        foreach (string keyboardInput in keyboardInputs)
        {
            Debug.Log(keyboardInput);
        }
    }

 

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            PrintKeyboardInputHistory();
        }
    }
}
