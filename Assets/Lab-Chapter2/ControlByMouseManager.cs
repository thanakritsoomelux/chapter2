using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlByMouseManager : MonoBehaviour
{
    public float m_MovementStep = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        

        if (Input.GetMouseButtonDown(0))
        {
            this.transform.Translate(-m_MovementStep, 0, 0);
        }
        else if (Input.GetMouseButtonDown(1))
        {
            this.transform.Translate(m_MovementStep , 0, 0);
        }

        if (Input.GetMouseButtonUp(0))
        {
            this.transform.Translate(m_MovementStep , 0, 0);
        }
        else if (Input.GetMouseButtonUp(1))
        {
            this.transform.Translate(-m_MovementStep, 0, 0);
        }
    }

    private void ShootABulletInForwardDirection(Vector3 forceModifier, PrimitiveType type = PrimitiveType.Sphere,
        float forceMagnitude = 1)
    {
        var newGameObject = GameObject.CreatePrimitive(type);

        newGameObject.transform.position = transform.position + transform.forward * 0.15f;
        
        var rigidbody = newGameObject.AddComponent <Rigidbody >();
        rigidbody.mass = 0.15f;
        
        Vector3 shootingDirection = (forceModifier + transform.forward) *forceMagnitude;
        rigidbody.AddForce(shootingDirection ,ForceMode.Impulse);
        
        Destroy(newGameObject , 3);
        
    }
}
